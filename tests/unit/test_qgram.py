"""Unit tests for qgram-based indexer."""
import io
import os
from contextlib import contextmanager

import abydos.distance
import pytest
from src.fastfuzzy import QGramIndex


@contextmanager
def does_not_raise():
    yield None


class TestQGramIndex:
    @pytest.mark.parametrize(
        "input,entries,expected,expected_exception",
        [
            ("", set(), ("", 0.0), pytest.raises(ValueError)),
            ("a", {"a"}, ("a", 1.0), does_not_raise()),
            ("ab", {"abcd"}, ("abcd", 0.25), does_not_raise()),
            ("ab", {"ef", "abcd"}, ("abcd", 0.25), does_not_raise()),
            ("abc", {"def"}, (None, 0.0), does_not_raise()),
        ],
    )
    def test_max_sim(self, input, entries, expected, expected_exception):
        with expected_exception:
            assert QGramIndex(entries=entries).max_sim(input) == expected

    @pytest.mark.parametrize(
        "entries,expected,exception",
        [
            (set(), dict(), pytest.raises(ValueError)),
            (
                ["token1", "token2"],
                {
                    "$$t": 2,
                    "$to": 2,
                    "tok": 2,
                    "oke": 2,
                    "ken": 2,
                    "en1": 1,
                    "en2": 1,
                    "n1#": 1,
                    "n2#": 1,
                    "1##": 1,
                    "2##": 1,
                },
                does_not_raise(),
            ),
        ],
    )
    def test_build_index(self, entries, expected, exception):
        with exception:
            index = QGramIndex(entries=entries)
            for qgram, indices in index._qgram_index.items():
                assert len(indices) == expected[qgram]
            for qgram, expected_count in expected.items():
                assert len(index._qgram_index[qgram]) == expected_count

    @pytest.mark.parametrize(
        "entries,input,expected",
        [
            ({"test token"}, "test token", ["test token"]),
            ({"test token"}, "abc", []),
            ({"test token", "abc"}, "test token", ["test token"]),
            ({"test token", "test 123"}, "test token", ["test token", "test 123"]),
        ],
    )
    def test_candidates(self, entries, input, expected):
        assert QGramIndex(entries=entries)._candidates(input) == expected

    @pytest.mark.parametrize(
        "entries1,entries2,q1,q2,expected_entries,exception",
        [
            (["token1"], ["token2"], 2, 2, ["token1", "token2"], does_not_raise()),
            (
                ["token1", "token3"],
                ["token2", "token3"],
                2,
                2,
                ["token1", "token2", "token3"],
                does_not_raise(),
            ),
            (
                ["token1", "token3"],
                ["token2", "token3", "token4"],
                2,
                2,
                ["token1", "token2", "token3", "token4"],
                does_not_raise(),
            ),
            (
                ["token1"],
                ["token2"],
                1,
                2,
                ["token1", "token2"],
                pytest.raises(ValueError),
            ),
        ],
    )
    def test_add(self, entries1, entries2, q1, q2, expected_entries, exception):
        with exception:
            assert (
                sorted(
                    (
                        QGramIndex(entries=entries1, q=q1)
                        + QGramIndex(entries=entries2, q=q2)
                    )._entry_list
                )
            ) == expected_entries

    @pytest.mark.parametrize(
        "cmp,token1,token2,expected_sim",
        [
            (abydos.distance.QGram, "abcd", "bcde", 0.09090909090909094),
            (abydos.distance.PositionalQGramDice, "abcd", "bcde", 0.16666666666666666),
        ],
    )
    def test_cmp(self, cmp, token1, token2, expected_sim):
        assert QGramIndex(entries=[token1], cmp=cmp).max_sim(token2) == (
            token1,
            expected_sim,
        )

    def test_from_file(self):
        index = QGramIndex.from_file(
            io.StringIO(f"token1{os.linesep}token2", newline=os.linesep)
        )
        assert sorted(index._entry_list) == ["token1", "token2"]

    def test_from_path(self, tmpdir):
        tmp_file = tmpdir.join("file.txt")
        with open(tmp_file, "w") as f:
            f.write(f"token1{os.linesep}token2")
        index = QGramIndex.from_path(tmp_file)
        assert sorted(index._entry_list) == ["token1", "token2"]

    @pytest.mark.parametrize(
        "entries, expected_length",
        [(["token"], 1), (["token1", "token2"], 2), (["token1", "token1"], 1)],
    )
    def test_len(self, entries, expected_length):
        assert len(QGramIndex(entries=entries)) == expected_length